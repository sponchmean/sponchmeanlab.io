'use strict';

var granimInstance = new Granim({
    element: '#canvas-basic',
    name: 'basic-gradient',
    direction: 'left-right', // 'diagonal', 'top-bottom', 'radial'
    opacity: [1, 1],
    isPausedWhenNotInView: true,
    states : {
        "default-state": {
            gradients: [
                ['#AA076B', '#61045F'],
                ['#02AAB0', '#00CDAC'],
                ['#DA22FF', '#9733EE']
            ]
        }
    }
});

document.addEventListener('mousemove', function (e) {
    cursor.style = 'transform: scale(1)';
    cursor.style.top = (e.pageY - 10) + "px";
    cursor.style.left = (e.pageX - 10) + "px";
});

document.addEventListener('mouseout', function (e) {
    cursor.style = 'transform: scale(0)';
})

// Анимация фона при mousemove
var faceBirdX, faceBird = document.querySelector(".logo-img__wrapper");
function onFaceBird(e) {
    faceBirdX = .02 * (e.x - window.innerWidth / 2);
    faceBird.style = "transform: translateX(" + faceBirdX + "px)";
}
document.querySelector(".logo").addEventListener("mousemove", onFaceBird);


var worksBackX, worksSVG = document.querySelector('.shape-overlays');
document.querySelector(".works").addEventListener("mousemove", function(e){
    worksBackX = .02 * (e.y - window.innerHeight / 2);
    worksSVG.style = "transform: translateY(" + worksBackX + "px)";
})

// Clip-path в контактах
document.querySelector(".contact").addEventListener('mousemove', function(e) {
    // процент смешения курсора от верхнего края элемента
    var width = window.innerWidth;
    var mouseX = e.x;
    var percentX = (mouseX * 100) / width;
    // процент смешения курсора от левого края элемента
    var height = window.innerHeight;
    var mouseY = e.y;
    var percentY = (mouseY * 100) / height;
    // Обновляем стили для враппера текста
    $('.bg-text').css("--maskX", percentX + '');
    $('.bg-text').css("--maskY", percentY + '');
})

// One Page scroll
window.addEventListener("mousewheel", function (e) {

    // Определяем delta
    e = e || window.event;
    var delta = e.deltaY || e.detail || e.wheelDelta;

    // Декларируем переменные с DOM
    var activePage = document.querySelector('.page-active');
    var actPageData = activePage.getAttribute('data-id');

    // Смотрим, в какую сторону был скролл
    if (delta > 0) { // Up
        nextSlide(e, actPageData, activePage);
    } else if (delta < 0) { // Down
        prevSlide(e, actPageData, activePage)
    }
});

function nextSlide(e, n, o) {
    var actPageNum = 2;
    // Сравнение с позицией
    // Управление состоянием блока
    if (n == actPageNum) {
        o.classList.remove("page-active");
        o.nextElementSibling.classList.add('page-active');
    } else if (n > actPageNum) {
        return false;
    } else if (n < actPageNum) {
        o.classList.remove("page-active");
        o.nextElementSibling.classList.add('page-active');
    };
}

function prevSlide(e, n, o) {
    var actPageNum = 2;
    // Сравнение с позицией
    // Управление состоянием блока
    if (n == actPageNum) {
        o.classList.remove("page-active");
        o.previousElementSibling.classList.add('page-active');
    } else if (n < actPageNum) {
        return false;
    } else if (n > actPageNum) {
        o.classList.remove("page-active");
        o.previousElementSibling.classList.add('page-active');
    };
}

// Портфолио слайдер
$('.slider-nav__link').click(function(e){
    var sliderId = $(this).attr('data-id').slice(-1);
    console.log(sliderId)
    $('.slider-list').animate({
        left: -100 * sliderId + '%'
    }, 200);
    $('.slider-nav__link.is-active').removeClass('is-active');
    $(this).addClass('is-active');
}); 